# Howto Bobot this lab

1. You cna modify PipelineWrapper as much as you can, but it cannot use any hardcoded paths within fit/predict functions
2. You can print ontly to standard error output, **do not print anything to standard output**
3. You can add as many files as you want to **src** directory 
4. Have fun :)

# Prizes
Scoreboard: [See here](https://gitlab.com/uj-courses/2023-2024/wpdm/warmup/-/wikis/Scoreboard)
| Place | Points |
| ------ | ------ |
| :first_place:  |    2     |
| :second_place: |    1     |
| :third_place:  |   0.5     |

## More details
- Each repository should contain a model.py file located in the src directory, as well as a requirements.txt file located in the main directory. Additionally, the project can have any number of other files.
- In the model.py file, there is a class called PipelineWrapper that performs two tasks: fit and predict. Assume that everything related to training and prediction is encapsulated within this class. Specifically, the parameters for the fit and predict functions are raw data, which should be prepared and utilized for training and prediction within these functions.
- The results of the project will be visible in the GRADE.md file in the student’s repository. However, I reserve the right to exclude information about current results.
- The scoreboard will be accessible on the wiki page for the specific task. For example, for the WarmUP task, the scoreboard will be on this wiki page: Scoreboard.
- To be eligible in the ranking, you must achieve an r2_score greater than 0.00.

## Data Set Characteristics:

- Number of Instances: 20640

- Number of Attributes: 8 numeric, predictive attributes and the target

- Attribute Information:
    - MedInc:        median income in block group
    - HouseAge:      median house age in block group
    - AveRooms:      average number of rooms per household
    - AveBedrms:     average number of bedrooms per household
    - Population:    block group population
    - AveOccup:      average number of household members
    - Latitude:      block group latitude
    - Longitude:     block group longitude

- Missing Attribute Values: None

This dataset was obtained from the StatLib repository.
https://www.dcc.fc.up.pt/~ltorgo/Regression/cal_housing.html

The target variable is the median house value for California districts,
expressed in hundreds of thousands of dollars ($100,000).

This dataset was derived from the 1990 U.S. census, using one row per census
block group. A block group is the smallest geographical unit for which the U.S.
Census Bureau publishes sample data (a block group typically has a population
of 600 to 3,000 people).

A household is a group of people residing within a home. Since the average
number of rooms and bedrooms in this dataset are provided per household, these
columns may take surprisingly large values for block groups with few households
and many empty houses, such as vacation resorts.