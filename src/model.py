from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler
import numpy as np

class PipelineWrapper:
    def __init__(self):
        self.model = LinearRegression()
        self.scaler = StandardScaler()
    def fit(self,X,y):
        """

        :param X: pandas Dataframe or nunpy array with features, not including target variable
        :param y: np.array containing target variable
        """
        pass


    def predict(self,X):
        """

        :param X: pandas Dataframe or numpy array with features, not including target variable
        :return: np.array with predictions or X
        """

        return np.ones(X.shape[0])
